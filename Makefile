SHELL = /bin/sh

# Output directory (HTML)
HTMLDIR := html

# Source files (Markdown)
MD := $(wildcard *.md) $(wildcard assets/*.md)

# Output files (HTML)
HTML := $(addprefix $(HTMLDIR)/,$(patsubst %.md,%.html,$(MD)))

.PHONY: html
html: $(HTML)

.PHONY: clean
clean:
	rm -rf $(HTMLDIR)

$(HTMLDIR)/pandoc.css: pandoc.css
	mkdir -p $(HTMLDIR)
	cp $< $@

$(HTMLDIR)/%.html: %.md $(HTMLDIR)/pandoc.css
	mkdir -p $(HTMLDIR)/$(dir $<)
	pandoc -t html4 --css pandoc.css $< -so $@
	sed -i s/\.md/\.html/g $@

# Remove double spaces/trailing whitespace, and convert newlines to LF
.PHONY: format
format:
	sed -i 's/\.  /\. /g' $(MD)
	sed -i 's/,  /, /g' $(MD)
	sed -i 's/\*  /\* /g' $(MD)
	sed -i 's/-  /- /g' $(MD)
	sed -i 's/+  /+ /g' $(MD)
	sed -i 's/\s*$$//g' $(MD)
	dos2unix -q $(MD)
