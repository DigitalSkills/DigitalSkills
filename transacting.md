# Transacting

The skills required to register and apply for services, buy and sell goods and services, and administer and manage transactions online.

## Skills for life

I can:

* set up an account online, using appropriate websites or Apps, that enables me to buy goods or services
* access and use public services online, including filling in forms
* use different payment systems, such as credit/debit card, direct bank transfer, and phone accounts, to make payments for goods or services online
* upload documents and photographs when this is required to complete an online transaction
* fill in online forms when required to complete an online transaction
* manage my money and transactions online and securely, such as my bank, through the use of websites or apps

## Skills for life examples

I can:

* set up online accounts for public services such as with your local council or a government department
* set up online accounts with retailers to order and pay for goods online such as through Amazon or eBay
* use travel websites and apps to book tickets and make reservations
* make a GP appointment online
* complete online forms to apply for a television license or road tax
* set up and use online and telephone banking through websites or apps, keeping access information secure
* upload a CV to an online recruitment site
* complete an online application form, for example for a job

The information presented above is done so under the conditions set out on the [source](./README.md) page

<hr>

## How we can do this with Free, Libre, and Open Source software and privacy friendly alternatives

We can advocate the use of free tools, free software, non profits and other providers.

* [GNU Taler](https://taler.net/en/index.html)
  * GNU Taler launched at [Bern University of Applied Sciences](https://www.fsf.org/bulletin/2020/fall/free-software-payment-system-launches-at-swiss-university)

* [Liberapay](https://en.liberapay.com/) (non profit), for recurring payments

We can also use cryptocurrencies:

* [Bitcoin](https://bitcoin.org)
* [Monero](https://getmonero.org) - anonymous and untraceable
