# Communicating

The skills required to communicate, collaborate, and share information.

## Skills for life

I can:

* understand the importance of communicating securely
* set up an email account
* communicate with others digitally using email and other messaging apps
* use word processing applications to create documents
* share documents with others by attaching them to an email
* communicate with friends and family using video tools
* post messages, photographs, videos or blogs on social media platforms

## Skills for life examples

I can:

1. set up a group on messaging platforms, such as WhatsApp or Messenger, to talk to friends or family members
2. use word processing software to create a CV or a letter
3. send photographs and other documents to friends and family as an email attachment
4. set up and use video-telephony products such as Facetime or Skype for video communications with friends and family
5. be a member of and manage personal networking sites, such as Facebook
6. post appropriately on social media, visit and post to forums such as Mumsnet or Reddit

The information presented above is done so under the conditions set out on the [source](./README.md) page

<hr/>

## How we can do this with Free, Libre, and Open Source software and privacy friendly alternatives

1. Use Privacy Friendly & Free Software messaging services
   * [Matrix](https://matrix.org/)
   * [Telegram](https://telegram.org/)
   * [Signal](https://signal.org/en/)
   * [Jami](https://jami.net/)
   * [Internet Relay Chat](http://www.ircbeginner.com/)
   * [XMPP (Messaging protocol)](https://xmpp.org/)<br/>
     XMPP clients:
     * [Gajim](https://gajim.org/)
     * [Conversations](https://conversations.im/)
     * [Dino](https://dino.im/)

2. Use Free/Libre/Open Source document formats and word processors
   * [LibreOffice](https://www.libreoffice.org/)
     * [LibreOffice Templates](https://www.libreofficetemplates.net/)
     * [LibreOffice basics](https://personaljournal.ca/libreoffice/)
     * [LibreOffice Documentation](https://documentation.libreoffice.org/en/english-documentation/)
   * [The Document foundation](https://www.documentfoundation.org/)
   * [Using Open Document Formats (ODF) in your organisation](https://www.gov.uk/guidance/using-open-document-formats-odf-in-your-organisation)

   **Collaborative editing**

   * [EtherPad](https://etherpad.org/) - Collaborative Editor
   * [EtherCalc](https://ethercalc.net/) - Collaborative Spreadsheet
     * Both the above come as part of Disroot, Cryptpad (below) also has
       collaborative features)

   * [CryptPad](https://cryptpad.fr/): text, spreadsheet, presentation, cloud and more

   **Typesetting**

   * [LaTeX](https://www.latex-project.org/)
     * [LaTeX Templates](https://www.latextemplates.com/)
     * [Overleaf - collaborative editor](https://www.overleaf.com/)
     * [Overleaf / LaTeX documentation](https://www.overleaf.com/learn)

3. Attach document to e-mail
   * [Disroot](https://disroot.org/en)
   * [ProtonMail](https://protonmail.com/)

4. Use Free/Libre & Privacy friendly alternative videoconferencing software
   * [Jitsi](https://jitsi.org/)<br/>
     Jitsi servers:
     * [meet.jit.si](https://meet.jit.si/)
     * [Disroot](https://calls.disroot.org)
   * [BigBlueButton](https://bigbluebutton.org/)<br/>
     BigBlueButton servers:
     * [FairCam](https://22.faircam.net/)
     * [Senfcall](https://senfcall.de/en)

5. Join the Fediverse, decentralised social media
   * [Fediverse](https://fediverse.party/)
   * [What is the Fediverse](https://torresjrjr.com/archive/2020-07-20-what-is-the-fediverse)
   * [Mastodon](https://joinmastodon.org/)
   * [Mastodon user guide](https://docs.framasoft.org/en/mastodon/User-guide.html)
   * [Lemmy](https://join.lemmy.ml/): Reddit alternative, link aggregator

6. Use Free/Libre alternative forum software
   * [Discourse](https://www.discourse.org/)
